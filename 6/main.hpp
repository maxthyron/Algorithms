#ifndef MAIN_HPP
#define MAIN_HPP

#define TOWN_COUNT 10
#define ANT_COUNT 10
#define ELITE_ANT 3
#define MAX_DISTANCE 9
#define GENERATIONS 1
#define P_COEF 0.2
#define Q 10

#endif