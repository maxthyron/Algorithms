void planRoute(Ant& ant, int** townsMatrix, float** pheromonMatrix, float alpha, float beta)
{
    float top = 0;
    float bottom = 0;
    int startID;
    int bestID = -1;
    float maxP = -1;
    std::vector<int> destinations = ant.getDestinations();
    for (int j = 0; j < TOWN_COUNT - 1; j++)
    {
        maxP = -1;
        bestID = -1;
        destinations = ant.getDestinations();
        std::vector<int> route = ant.getRoute();
        startID = route[route.size() - 1];
        for (int i = 0; i < destinations.size(); i++)
        {
            int endID = destinations[i];
            top = float(pow(pheromonMatrix[startID][endID], alpha)) * float(pow(1 / townsMatrix[startID][endID], beta));

            for (int k = 0; k < destinations.size(); k++)
            {
                int tempID = destinations[k];
                float temp = (float)(1.0 / townsMatrix[startID][tempID]);
                bottom += pow(pheromonMatrix[startID][tempID], alpha) * pow(temp, beta);
            }

            float P = float(top / bottom);

            if (maxP < P)
            {
                maxP = P;
                bestID = destinations[i];
            }
        }
        ant.updateRoad(bestID, townsMatrix[startID][bestID]);
    }
}
