import random
import time

import files
import sorts


def generateArray(SIZE=10):
    arr = [random.randint(-9, 9) for _ in range(SIZE)] 
    return arr 

def increasingArray(SIZE=10):
    return [i for i in range(SIZE)]

def decreasingArray(SIZE=10):
    return [i for i in reversed(range(SIZE))]


def check(method, N=1, SIZE=10):
    print(method.__name__)
    for i in range(N):
        tempArray = generateArray(SIZE=SIZE)
        result = method(tempArray)
        print("Initial:", tempArray)
        print("Result :", result)

        passed = True
        for i in range(len(result) - 1):
            if result[i] > result[i + 1]:
                print("Failed", result[i], result[i + 1])
                passed = False
        if passed:
            print("Passed")
        print()


def timeTest(filename="time.csv", ITERATIONS=100, N=1000, getArray=generateArray):
    print(filename[4:-4].upper() + "\n")
    def showPercentage(denominator):
        numerator = s * len(sorts.methods) * ITERATIONS + m * ITERATIONS + i + 1
        print("{:3.2f}%".format(100 * numerator / denominator), end="\r")

    methods = sorts.methods
    data = []

    data.append(["Size"] + [method.__name__ for method in methods])

    START = 100
    END = N + 100
    STEP = 100
    TOTAL = (END - START) / STEP

    denominator = TOTAL * len(methods) * ITERATIONS

    for s, SIZE in enumerate(range(START, END, STEP)):
        data.append([SIZE])
        for m, method in enumerate(methods):
            tempTime = 0
            for i in range(ITERATIONS):
                tempArray = getArray(SIZE=SIZE)
                start = time.process_time()
                method(tempArray)
                stop = time.process_time()
                tempTime += (stop - start)
                showPercentage(denominator)
            tempTime /= ITERATIONS
            data[1 + s].append(tempTime)

    files.writecsv(filename, data)

def test():
    for method in sorts.methods:
        check(method)

if __name__ == "__main__":
    timeTest("timerandom.csv", getArray=generateArray)
    
    # timeTest("timeincrease.csv", getArray=increasingArray)
    # timeTest("timedecrease.csv", getArray=decreasingArray)
