import pandas as pd
import matplotlib.pyplot as plt

import files


def showGraph(data):
    methods = data[0][1:]
    results = [[] for i in range(len(methods))]
    measures = []
    for row in range(1, len(data)):
        measures.append(int(data[row][0]))
        for i in range(len(methods)):
            results[i].append(float(data[row][1 + i]))

    table = pd.DataFrame(data[1:], columns=data[0])
    print(table)

    plt.xlabel(data[0][0])
    plt.ylabel("Time(ms)")
    for i in range(len(methods)):
        plt.plot(measures, results[i], label=methods[i])

    plt.legend()
    plt.show()

def graph(filename="time.csv"):
    data = files.readcsv(filename)
    showGraph(data)

if __name__ == "__main__":
    graph("timerandom.csv")
