import csv

def writecsv(filename, data):
    with open(filename, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter='|', quoting=csv.QUOTE_MINIMAL)
        writer.writerows(data)


# def writecsv(filename, data):
#     with open(filename, 'w', newline='') as csvfile:
#         writer = csv.writer(csvfile, delimiter='|', quoting=csv.QUOTE_MINIMAL)
#         writer.writerows(data)

def readcsv(filename):
    with open(filename, newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter='|')
        data = []
        for row in reader:
            data.append(row)

    return data


# def readcsv(filename):
#     with open(filename, newline='') as csvfile:
#         reader = csv.reader(csvfile, delimiter='|')
#         data = []
#         for row in reader:
#             data.append(row)

#     return data


if __name__ == "__main__":
    writecsv("hello.csv", [[0, 1, 2], [3, 25, 6]])
    data = readcsv("hello.csv")
    print(data)


