import random

import tests
import graphs


def main():
    tests.test()
    tests.timeTest()
    graphs.graph()


if __name__ == "__main__":
    main()
