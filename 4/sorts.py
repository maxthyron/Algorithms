# Function to do insertion sort with barrier
def insertionBarrierSort(array):  # insert sort method
    arr = list([0] + array) # creates copy and adds barried

    for i in range(2, len(arr)):    
        if arr[i - 1] > arr[i]:
            arr[0] = arr[i]
            j = i - 1
            while arr[j] > arr[0]:
                arr[j + 1] = arr[j]
                j-= 1
            arr[j + 1] = arr[0]

    return arr[1:] 


# Function to do insertion sort 
def insertionSort(array): 
    arr = list(array) # creates copy

    for i in range(1, len(arr)): 
        key = arr[i] 
        j = i - 1
        while j >= 0 and key < arr[j]: 
            arr[j + 1] = arr[j] 
            j -= 1
        arr[j+1] = key 
    
    return arr


# Function to do comb sort 
def combSort(array): 
    arr = list(array) # creates copy

    n = len(arr) 
    gap = n 
    swapped = True
  
    while gap != 1 or swapped == 1: 
        gap = int(1 if gap < 1 else (gap * 10) / 13)
        swapped = False
        for i in range(0, n-gap): 
            if arr[i] > arr[i + gap]: 
                arr[i], arr[i + gap] = arr[i + gap], arr[i] 
                swapped = True
    return arr

methods = [insertionBarrierSort, insertionSort, combSort]
