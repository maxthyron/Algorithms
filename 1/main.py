import random
import string
import time
import matplotlib.pyplot as plt


from files import *
from prints import *
from algorithms import *
from settings import *


def menu():
    while True:
        printMenu()
        choice = input("> ")

        try:
            index = int(choice)
        except ValueError:
            print("Error")
            continue

        if index < 0 or index > menuLength:
            print("Index out of range")
        elif index == 0:
            break
        elif index == 1:
            calculateDistance(levenshtein)
        elif index == 2:
            calculateDistance(damerauLevenshtein)
        elif index == 3:
            calculateDistance(levenshteinRecursiveMain)
        elif index == 4:
            showExamples()
        elif index == 5:
            test()
        elif index == 6:
            graph()
        input("\nContinue...")


def calculateDistance(method, strings=None, printOut=True):
    if not strings:
        strings = getStrings()
    n = len(strings[0])
    m = len(strings[1])
    distances = getDistances(n, m)
    collisions = [[0] * (n + 1)] * (m + 1)
    distances, result = method(strings, distances, collisions)
    if printOut:
        printMatrix(strings, distances)
        printResult(result)


def getDistances(n, m):
    distances = [list(range(n + 1))]
    for i in range(1, m + 1):
        distances.append([i] + [0] * n)
    return distances


def showExamples():
    examples = getExamples()
    printHeader()
    for i in range(len(examples)):
        pair = examples[i]
        results = []
        n = len(pair[0])
        m = len(pair[1])
        collisions = [[0] * (n + 1)] * (m + 1)
        for method in methods:
            distances = getDistances(n, m)   

            distances, result = method(pair, distances, collisions)
            results.append(result)

        printLine(pair, results)


def test():
    while True:
        try:
            stringLength = int(input("Enter string length> "))
            iterationsAmount = int(input("Enter amount of iterations> "))
            break
        except ValueError:
            print("Error")
            continue
    
    results = []

    print()
    for j, method in enumerate(methods):
        totalTime = .0
        for i in range(iterationsAmount):
            strings = [generateString(stringLength),
                       generateString(stringLength)]
            n = len(strings[0])
            m = len(strings[1])
            collisions = [[0] * (n + 1)] * (m + 1)
            distances = getDistances(n, m)   

            startTime = time.process_time()
            method(strings, distances, collisions)
            endTime = time.process_time() - startTime

            totalTime += endTime
            print("%3.2f%s" % ((iterationsAmount * j + (i + 1)) /
                                methodsLength / iterationsAmount * 100, "%"), end="\r")
        totalCollisions = 0
        for i in range(n + 1):
            for j in range(m + 1):
                totalCollisions += collisions[i][j]
        results.append([totalTime, totalCollisions]) 
    print("\n")
    printTime(results)

    return results


def graph():
    auto = False
    if not auto:
        startLength = 100
        endLength = 600
        step = 50
        iterationsAmount = 100
    else:
        while True:
            try:
                startLength = int(input("Enter start length> "))
                endLength = int(input("Enter end length> "))
                step = int(input("Enter step> "))
                iterationsAmount = int(input("Enter amount of iterations> "))
                break
            except ValueError:
                print("Error")
                continue
 
    levArray = []
    damArray = []
    lengths = []

    for i, l in enumerate(range(startLength, endLength, step)):
        firstWord =  generateString(l)
        secondWord = generateString(l)
        levTicks = 0
        damTicks = 0

        for _ in range(iterationsAmount):
            start = time.process_time()
            calculateDistance(levenshtein, (firstWord, secondWord), False)
            end = time.process_time() - start

            levTicks += end

            start = time.process_time() 
            calculateDistance(damerauLevenshtein, (firstWord, secondWord), False)
            end = time.process_time() - start

            damTicks += end

            print("{:^3.2f}%".format((iterationsAmount * i + _ + 1) / (iterationsAmount * (endLength - startLength) // step) * 100), end="\r")

        print(levTicks, damTicks)
        levArray.append(levTicks / iterationsAmount)
        damArray.append(damTicks / iterationsAmount)
        lengths.append(l)

    plt.plot(lengths, levArray, "ro-",
             lengths, damArray, "bo-")

    plt.legend(["Levenshtein",
                "Damerau-Levenshtein"],
                loc="upper left")

    plt.show()

   
def generateString(length):
    return "".join(random.choice(string.ascii_lowercase) for c in range(length))


def getStrings():
    print()
    first = input("Enter first string> ")
    second = input("Enter second string> ")

    return first, second


if __name__ == "__main__":
    menuLength = 6
    methods = [levenshtein, damerauLevenshtein, levenshteinRecursiveMain]
    methodsLength = len(methods)
    menu()

