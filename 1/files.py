
def getExamples():
    with open("examples.txt", 'r') as f:
        examples = []
        for line in f:
            pair = line.rstrip().split(" ")
            if pair != [""]:
                examples.append(pair)
    
    return examples


