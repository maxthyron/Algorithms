def levenshtein(strings, distances, collisions):
    n = len(strings[0])
    m = len(strings[1])
    for i in range(1, m + 1):
        for j in range(1, n + 1):
            I = distances[i][j - 1] + 1
            D = distances[i - 1][j] + 1
            M = distances[i - 1][j - 1] +\
            (0 if strings[0][j - 1] == strings[1][i - 1] else 1)

            distances[i][j] = min(I, D, M)
            collisions[i][j] += 1

    return distances, distances[-1][-1]

def damerauLevenshtein(strings, distances, collisions):
    n = len(strings[0])
    m = len(strings[1])
    for i in range(1, m + 1):
        for j in range(1, n + 1):
            I = distances[i][j - 1] + 1
            D = distances[i - 1][j] + 1
            M = distances[i - 1][j - 1] +\
            (0 if strings[0][j - 1] == strings[1][i - 1] else 1)

            if i > 1 and j > 1 and (strings[1][i - 2] == strings[0][j - 1] and strings[1][i - 1] == strings[0][j-2]):
                T = distances[i - 2][j - 2] + 1
                distances[i][j] = min(I, D, M, T)
            else:
                distances[i][j] = min(I, D, M)
            collisions[i][j] += 1

    return distances, distances[-1][-1]

def levenshteinRecursiveMain(strings, distances, collisions):
    result = levenshteinRecursive(strings, distances, collisions)

    return distances, distances[-1][-1]

def levenshteinRecursive(strings, distances=None, collisions=None):
    if strings[0] == "":
        return len(strings[1])
    if strings[1] == "":
        return len(strings[0])

    result = min(levenshteinRecursive([strings[0][:-1], strings[1]], distances, collisions) + 1,
                 levenshteinRecursive([strings[0], strings[1][:-1]], distances, collisions) + 1,
                 levenshteinRecursive([strings[0][:-1], strings[1][:-1]], distances, collisions) +\
                 (0 if strings[0][-1] == strings[1][-1] else 1))

    distances[len(strings[1])][len(strings[0])] = result
    collisions[len(strings[1])][len(strings[0])] += 1

    return result

