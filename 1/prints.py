from settings import *

def printMenu():
    print()
    print("""\
1] Levenshtein(iterative)
2] Demirau-Levenshtein(iterative)
3] Levenshtein(recursive)

4] Examples
5] Test
6] Graph

0] Exit
""")

def printMatrix(strings, distances):
    n = len(strings[0])
    m = len(strings[1])

    print()
    print("  @ ", end=("" if n != 0 else "\n"))
    for i in range(n):
        print(strings[0][i], end=(" " if i < n - 1 else "\n"))

    for i in range(m + 1):
        for j in range(n + 1):
            if j == 0:
                if i == 0:
                    print("@ ", end="")
                else:
                    print(strings[1][i - 1], end=" ")

            print(str(distances[i][j]), end=(" " if j != n else "\n"))

def printResult(result):
    print()
    print("Distance: %d" % (result))

def printLine(pair, results):
    print("{:^15s} - {:^15s}".format(pair[0], pair[1]), end="")

    for i in range(len(results)):
        print(" │ {:^10d}".format(results[i]), end = ("" if i < len(results) - 1 else "\n"))
    print("─" * 24 * methodsLength)

def printHeader():
    print()
    print("{:^15s} - {:^15s}".format("S1", "S2"), end="")
    for i in range(len(methodsLabels)):
        print(" │ {:^10s}".format(methodsLabels[i]), end = ("" if i < len(methodsLabels) - 1 else "\n"))
    print("═" * 24 * methodsLength)

def printTime(results):
    print("Methods   :  Time(ticks)   :   Collisions\n")
    for i in range(methodsLength):
        print(methodsLabels[i], " : {:^14.3f} : {:^14d}".format(results[i][0], results[i][1]))
