from collections import Counter
def call_counter(func):
    def helper(*args, **kwargs):
        helper.calls += 1
        key = str(args) + str(kwargs)
        helper.c[key] += 1
        return func(*args, **kwargs)
    helper.c = Counter()
    helper.calls = 0
    helper.__name__= func.__name__
    return helper


@call_counter
def levenshteinRecursive(strings, distances=None, collisions=None):
    print(strings[0], strings[1])

    if strings[0] == "":
        return len(strings[1])
    if strings[1] == "":
        return len(strings[0])

    result = min(levenshteinRecursive([strings[0][:-1], strings[1]], distances, collisions) + 1,
                 levenshteinRecursive([strings[0], strings[1][:-1]], distances, collisions) + 1,
                 levenshteinRecursive([strings[0][:-1], strings[1][:-1]], distances, collisions) +\
                 (0 if strings[0][-1] == strings[1][-1] else 1))

    if distances:
        distances[len(strings[1])][len(strings[0])] = result
    if collisions:
        collisions[len(strings[1])][len(strings[0])] += 1

    return result

if __name__ == "__main__":
    firstWord = "Python"
    secondWord = "P"
    
    print("\nResult:", levenshteinRecursive((firstWord, secondWord)))
    print()
    print("Recursive was called " + str(levenshteinRecursive.calls) + " times!")
