import matplotlib.pyplot as plt
from algorithms import *
from prints import *
from random import randint

def graph():
    sizes = [[], []]
    defaultTime = [[], []]
    winogradTime = [[], []]
    winogradImprTime = [[], []]

    with open('log.txt', 'r') as f:
        for i, line in enumerate(f):
            dimensions, default, winograd, imprwinograd = line.split("|")
            if i < 4:
                sizes[0].append(dimensions)
                defaultTime[0].append(float(default))
                winogradTime[0].append(float(winograd))
                winogradImprTime[0].append(float(imprwinograd))
            else:
                sizes[1].append(dimensions)
                defaultTime[1].append(float(default))
                winogradTime[1].append(float(winograd))
                winogradImprTime[1].append(float(imprwinograd))

    plt.subplot(211)
    plt.xlabel("Size of matrix")
    plt.ylabel("Time(ms)")
    plt.plot(sizes[0], defaultTime[0], "ro-",
             sizes[0], winogradTime[0], "bo-",
             sizes[0], winogradImprTime[0], "go-")

    plt.legend(["Default",
                "Winograd",
                "Improved Winograd"],
                loc="upper left")

    plt.subplot(212)
    plt.xlabel("Size of matrix")
    plt.ylabel("Time(ms)")
    plt.plot(sizes[1], defaultTime[1], "ro-",
             sizes[1], winogradTime[1], "bo-",
             sizes[1], winogradImprTime[1], "go-")

    plt.legend(["Default",
                "Winograd",
                "Improved Winograd"],
                loc="upper left")
    plt.show()


def testMult(a, b):
    print("First matrix:")
    printMatrix(a)
    print("Second matrix:")
    printMatrix(b)

    result = matrixMultDefault(a, b)
    print("Result(default)")
    printMatrix(result)

    result = matrixMultWinograd(a, b)
    print("Result(Winograd)")
    printMatrix(result)


def randomMult():
    n = randint(2, 5)
    m = randint(2, 5)
    a = generateMatrix(n, m)
    b = generateMatrix(m, n)
    testMult(a, b)


def main():
    for i in range(2):
        randomMult()

    graph()

if __name__ == "__main__":
    main()
