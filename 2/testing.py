from random import randint
from time import process_time
from algorithms import *


TIMES = 2


def random_matrix(n, m):
    return [[randint(0, 100) for i in range(m)] for j in range(n)]


def time_testing():
    print('...TIMING...\n\n')

    with open('log.txt', 'w') as f:

        for size in range(100, 500, 100):

            print('Multiplication of matrix {0} X {0} '.format(size))

            f.write("{0} X {0} |".format(size))

            a = random_matrix(size, size)
            b = random_matrix(size, size)

            t1, t2, t3 = 0, 0, 0

            for i in range(TIMES):

                start = process_time()
                res_class = matrixMultDefault(a, b)
                stop = process_time()

                t1 += (stop - start)

                start = process_time()
                res_wino = matrixMultWinograd(a, b)
                stop = process_time()

                t2 += (stop - start)

                start = process_time()
                res_wino_imprv = matrixMultWinogradImp(a, b)
                stop = process_time()

                t3 += (stop - start)


            f.write(" {0:<.5f}   | {1:<.5f}| {2:<.5f} \n".format(t1 * 1000/TIMES, t2 * 1000/TIMES, t3 * 1000/TIMES))

        for size in range(100, 500, 100):
                size += 1

                print('Multiplication of matrix {0} X {0} '.format(size))

                f.write("{0} X {0} |".format(size))

                a = random_matrix(size, size)
                b = random_matrix(size, size)

                t1, t2, t3 = 0, 0, 0

                for i in range(TIMES):

                    start = process_time()
                    res_class = matrixMultDefault(a, b)
                    stop = process_time()

                    t1 += (stop - start)

                    start = process_time()
                    res_wino = matrixMultWinograd(a, b)
                    stop = process_time()

                    t2 += (stop - start)

                    start = process_time()
                    res_wino_imprv = matrixMultWinogradImp(a, b)
                    stop = process_time()

                    t3 += (stop - start)


                f.write(" {0:<.5f}   | {1:<.5f}   | {2:<.5f}  \n".format(t1 * 1000 / TIMES, t2 * 1000 / TIMES, t3 * 1000 / TIMES))

    print('...end...\n\n')


if __name__ == '__main__':
    time_testing()
