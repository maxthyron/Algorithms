from random import randint
def createMatrix(n, m):
    newMatrix = []

    for i in range(m):
        newMatrix.append([0] * n)

    return newMatrix


def generateMatrix(n, m):
    newMatrix = []

    for i in range(m):
        newRow = []
        for j in range(n):
            newRow.append(randint(0, 9))
        newMatrix.append(newRow)

    return newMatrix




def matrixMultDefault(A, B):
    if len(B) != len(A[0]):
        print("Different dimension of the matrices.")
        return

    n = len(A)
    m = len(A[0])
    t = len(B[0])

    answer = [[0 for i in range(t)] for j in range(n)]
    for i in range(n): # N *
        for j in range(m): # M *
            for k in range(t): # T *
                answer[i][k] += A[i][j] * B[j][k] # 8
    return answer


def matrixMultWinograd(G, H):
    a = len(G)
    b = len(H)
    c = len(H[0])

    if b != len(G[0]):
        print("Different dimension of the matrics")
        return
    d = b // 2
    row_factor = [0 for i in range(a)]
    col_factor = [0 for i in range(c)]

    # Row Factor calc
    for i in range(a): # A *
        for j in range(d): # D *
            row_factor[i] += G[i][2 * j] * G[i][2 * j + 1] # 10

    # Col Factor calc
    for i in range(c): # C *
        for j in range(d): # D *
            col_factor[i] += H[2 * j][i] * H[2 * j + 1][i] # 10

    answer = [[0 for i in range(c)] for j in range(a)]
    for i in range(a): # A *
        for j in range(c): # C *
            answer[i][j] = - row_factor[i] - col_factor[j] # 7
            for k in range(d): # D *
                # 20
                answer[i][j] += ((G[i][2 * k] + H[2 * k + 1][j]) * (G[i][2 * k + 1] + H[2 * k][j]))
    # For odd matrix
    if b % 2: # 1 +
        for i in range(a): # A *
            for j in range(c): # C *
                answer[i][j] += G[i][b - 1] * H[b - 1][j] # 10

    return answer

def matrixMultWinogradImp(G, H):
    a = len(G)
    b = len(H)
    c = len(H[0])

    if b != len(G[0]):
        print("Different dimension of the matrices.")
        return
    d = b // 2
    row_factor = [0 for i in range(a)]
    col_factor = [0 for i in range(c)]

    # Row Factor calc
    for i in range(a): # A *
        row_factor[i] = sum(G[i][2 * j] * G[i][2 * j + 1] for j in range(d)) # 8D + 2

    # Col Factor calc
    for i in range(c): # C *
        col_factor[i] = sum(H[2 * j][i] * H[2 * j + 1][i] for j in range(d)) # 8D + 2

    answer = [[0 for i in range(c)] for j in range(a)]
    for i in range(a): # A *
        for j in range(c): # C *
            # 3 + 4 + 17D
            answer[i][j] = sum((G[i][2 * k] + H[2 * k + 1][j]) * (G[i][2 * k + 1] + H[2 * k][j]) for k in range(d))\
                           - row_factor[i] - col_factor[j]

    # For odd matrix
    if b % 2: # 1 +
        for i in range(a): # A *
            answer[i][j] = sum(G[i][b - 1] * H[b - 1][j] for j in range(c)) # 3 + 7C

    return answer
