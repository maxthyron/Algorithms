import bm
import kmp

textR = """Этот текст может содержать разные типы выражений, к примеру сокращения:
ч.т.д., и пр. и т.п и т.д. К тому же, он содержит почту: yourmail@mail.ru."""

textE = """This text may content all sorts of patterns and other
things, that may be used in this task of searching with regular
expressions e.t.c. You can contact me using this mail:
myemail@gmail.com."""

def highlight(text, indices, word):
    print("[" + word + "]", "--", indices)
    length = len(word)
    offset = 0
    for i in indices:
        start, middle, end = text[:i + offset], text[i + offset:i + length + offset], text[i + length + offset:]
        middle = "\033[4m" + middle + "\033[0m"
        text = start + middle + end
        offset += 8
    
    return text

def main():
    word = "к"
    result = list(kmp.search_string(textR, word))
    print("[KMP]\n" + highlight(textR, result, word))
    word = "mail"
    result = list(bm.string_search(textE, word))
    print()
    print("[BM]\n" + highlight(textE, result, word))


if __name__ == "__main__":
    main()