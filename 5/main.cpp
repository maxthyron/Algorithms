#include <cstring>
#include <iostream>
#include <math.h>
#include <time.h>
#include <fstream>

#include "matr.h"

#include "standart.h"
#include "multi.h"

#define N 10

clock_t time_meth(void (* meth)(int**, int**, int**, int, int, int), int** a,
                  int** b, int n);

clock_t time_meth_parallel(void (* meth)(int**, int**, int**, int, int, int,
                                         int), int** a, int** b, int n,
                           int cn_stream);

clock_t time_meth(void (* meth)(int**, int**, int**, int, int, int), int** a,
                  int** b, int n)
{
    clock_t tb, te, t;

    t = 0;

    for (int i = 0; i < N; i++)
    {
        tb = clock();

        meth(a, a, b, n, n, n);

        te = clock();

        t += te - tb;

        resetMatr(b, n, n);
    }

    return t / N;
}

clock_t time_meth_parallel(void (* meth)(int**, int**, int**, int, int, int, int), int** a, int** b, int n,
                           int cn_stream)
{
    clock_t tb, te, t;
    static int call_count = 0;
    call_count++;

    t = 0;

    for (int i = 0; i < N; i++)
    {
        tb = clock();

        meth(a, a, b, n, n, n, cn_stream);

        te = clock();

        t += te - tb;

        resetMatr(b, n, n);
    }

    std::cout << "\rFinished: " << (call_count % 2 ? "1/2" : "2/2\n") << std::flush;

    return t / N;
}

void test()
{
    char fileName[] = "../data/data.csv";
    int n = 500;
    clock_t t;

    std::ofstream file(fileName);
    if (!file)
    {
        std::cerr << "Cannot open " << fileName << std::endl;
        exit(1);

    }

    int** a = allocate_matrix(n, n);
    int** b = allocate_matrix(n, n);

    fillMatr(a, n, n);

    int i = 0;
    int j = int(pow(2, i));

    file << "Threads|Standard|Vinograd" << std::endl;

    while (i <= 2)
    {
        std::cout << "Calculating for " << j << " thread(s)." << std::endl;
        std::cout << "Finished: 0/2" << std::flush;
        t = time_meth_parallel(standart_parallel_mul, a, b, n, j);
        file << j << "|" << ((float)t) / CLOCKS_PER_SEC;
        t = time_meth_parallel(vinograd_parallel_mul, a, b, n, j);
        file << "|" << ((float)t) / CLOCKS_PER_SEC << std::endl;

        j = int(pow(2, ++i));
    }

    free_matrix(a, n);
    free_matrix(b, n);
}

void user()
{
    int n, m, q;

    std::cout << "Input n, m, q: ";
    std::cin >> n >> m >> q;
    std::cout << std::endl;

    int** a = allocate_matrix(n, m);
    int** b = allocate_matrix(m, q);
    int** c = allocate_matrix(n, q);

    fillMatr(a, n, m);
    printMatr(a, n, m);
    std::cout << std::endl;
    fillMatr(b, m, q);
    printMatr(b, m, q);

    int cn_stream;

    std::cout << std::endl << "Input count of stream: ";
    std::cin >> cn_stream;
    std::cout << std::endl;

    clock_t tb, te;

    tb = clock();
    vinograd_parallel_mul(a, b, c, n, m, q, cn_stream);
    te = clock();

    std::cout << "Matrix C:" << std::endl;
    printMatr(c, n, q);

    std::cout << std::endl << "Time: " << te - tb << std::endl;

    free_matrix(a, n);
    free_matrix(b, m);
    free_matrix(c, n);
}

int main(int argc, char** argv)
{
    if (argc > 1 && strcmp(argv[1], "test") == 0)
        test();
    else
        user();

    std::cout << std::endl << "Finish..." << std::endl;

    return 0;
}
