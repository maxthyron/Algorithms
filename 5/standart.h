#ifndef STANDART_H
#define STANDART_H

#include <thread>
#include <iostream>

void standart_mul(int **a, int **b, int **c, int n, int m, int q);
void standart_parallel_mul(int **a, int **b, int **c, int n, int m, int q,
                           int cn_stream);

#endif // STANDART_MUL_H
