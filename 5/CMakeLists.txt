cmake_minimum_required(VERSION 3.13)
project(Lab_5_AA)

set(CMAKE_CXX_STANDARD 17)

include_directories(.)

add_executable(Lab_5_AA
        main.cpp
        matr.cpp
        matr.h
        multi.cpp
        multi.h
        standart.cpp
        standart.h)
