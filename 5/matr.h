#ifndef MATR_H
#define MATR_H

#include <iostream>

int **allocate_matrix(int n, int m);
void free_matrix(int **data, int n);

void printMatr(int **data, int n, int m);
void scanMatr(int **data, int n, int m);

void fillMatr(int **data, int n, int m);
void resetMatr(int **data, int n, int m);

#endif // MATR_H
