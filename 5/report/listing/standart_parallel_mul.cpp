void standart_parallel_mul(int **a, int **b, int **c, int n, int m, int q, int
                           cn_stream)
{
    std::thread *threads = new std::thread [cn_stream];

    int count = n / cn_stream;
    int overRow = n % cn_stream;

    int offset = 0;

    if (not count)
        cn_stream = overRow;

    for (int i = 0; i < cn_stream; i++)
    {
        int cnRow = count + (i < overRow ? 1 : 0);
        threads[i] = std::thread(mul_row, a, b, c, offset, offset + cnRow,
                                 m, q);
        offset += cnRow;
    }

    for (int i = 0; i < cn_stream; i++)
        threads[i].join();

    delete [] threads;
}

void mul_row(int **a, int **b, int **c, int i_b, int i_e, int m, int q)
{
    for (int i = i_b; i < i_e; i++)
        for (int j = 0; j < m; j++)
            for (int k = 0; k < q; k++)
                c[i][k] += a[i][j] * b[j][k];
}
