void vinograd_mul(int **a, int **b, int **c, int n, int m, int q)
{
    int *mulH = new int [n];
    int *mulV = new int [q];

    for (int i = 0; i < n; i++)
    {
        mulH[i] = 0;
        for (int j = 0; j + 1 < m; j += 2)
            mulH[i] += a[i][j] * a[i][j + 1];
    }

    for (int i = 0; i < q; i++)
    {
        mulV[i] = 0;
        for (int j = 0; j + 1 < m; j += 2)
            mulV[i] += b[j][i] * b[j + 1][i];
    }

    for (int i = 0; i < n; i++)
        for (int j = 0; j < q; j++)
        {
            c[i][j] = -(mulH[i] + mulV[j]);

            for (int k = 0; k + 1 < m; k += 2)
                c[i][j] += (a[i][k] + b[k + 1][j]) * (a[i][k + 1] + b[k][j]);

            c[i][j] += m % 2 ? a[i][m - 1] * b[m - 1][j] : 0;
        }

    delete [] mulH;
    delete [] mulV;
}
