void vinograd_parallel_mul(int **a, int **b, int **c, int n, int m, int q,
                           int cn_stream)
{
    std::thread *threads = new std::thread [cn_stream];

    int *mulH = new int [n];
    int *mulV = new int [q];

    int count = q / cn_stream;
    int over = q % cn_stream;
    int new_cn_stream = cn_stream;
    int offset = 0;

    if (not count)
        new_cn_stream = over;

    for (int i = 0; i < new_cn_stream; i++)
    {
        int cn = count + (i < over ? 1 : 0);
        threads[i] = std::thread(first_part_mulV, mulV, b, offset, offset + cn, m);
        offset += cn;
    }

    count = n / cn_stream;
    over = n % cn_stream;
    offset = 0;

    if (not count)
        new_cn_stream = over;

    for (int i = 0; i < new_cn_stream; i++)
    {
        threads[i].join();

        int cn = count + (i < over ? 1 : 0);
        threads[i] = std::thread(first_part_mulH, mulH, a, offset, offset + cn, m);
        offset += cn;
    }

    offset = 0;
    for (int i = 0; i < new_cn_stream; i++)
    {
        threads[i].join();

        int cn = count + (i < over ? 1 : 0);
        threads[i] = std::thread(second_part, a, b, c, mulH, mulV,
                                 offset, offset + cn, q, m);
        offset += cn;
    }

    for (int i = 0; i < new_cn_stream; i++)
        threads[i].join();

    delete [] mulH;
    delete [] mulV;

    delete [] threads;
}

void first_part_mulH(int *mulH, int **a, int i_b, int i_e, int m)
{
    for (int i = i_b; i < i_e; i++)
    {
        mulH[i] = 0;
        for (int j = 0; j + 1 < m; j += 2)
            mulH[i] += a[i][j] * a[i][j + 1];
    }
}

void first_part_mulV(int *mulV, int **b, int i_b, int i_e, int m)
{
    for (int i = i_b; i < i_e; i++)
    {
        mulV[i] = 0;
        for (int j = 0; j + 1 < m; j += 2)
            mulV[i] += b[j][i] * b[j + 1][i];
    }
}

void second_part(int **a, int **b, int **c, int *mulH, int *mulV,
                int i_b, int i_e, int q, int m)
{
    for (int i = i_b; i < i_e; i++)
        for (int j = 0; j < q; j++)
        {
            c[i][j] = -(mulH[i] + mulV[j]);

            for (int k = 0; k + 1 < m; k += 2)
                c[i][j] += (a[i][k] + b[k + 1][j]) * (a[i][k + 1] + b[k][j]);

            c[i][j] += m % 2 ? a[i][m - 1] * b[m - 1][j] : 0;
        }
}
