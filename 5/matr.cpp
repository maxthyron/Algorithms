#include "matr.h"
#include <iostream>

int **allocate_matrix(int n, int m)
{
    int **data = new int *[n];

    for (int i = 0; i < n; i++)
    {
        data[i] = new int [m];

        for (int j = 0; j < m; j++)
            data[i][j] = 0;
    }

    return data;
}

void free_matrix(int **data, int n)
{
    for (int i = 0; i < n; i++)
        delete [] data[i];

    delete [] data;
}


void printMatr(int **data, int n, int m)
{
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
            std::cout << data[i][j] << " ";
        std::cout << std::endl;
    }
}

void scanMatr(int **data, int n, int m)
{
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            std::cin >> data[i][j];
}

void fillMatr(int **data, int n, int m)
{
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            data[i][j] = i + j;
}

void resetMatr(int **data, int n, int m)
{
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            data[i][j] = 0;
}
