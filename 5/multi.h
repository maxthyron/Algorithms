#ifndef MULTI_H
#define MULTI_H

#include <thread>
#include <iostream>

void vinograd_mul(int **a, int **b, int **c, int n, int m, int q);
void vinograd_parallel_mul(int **a, int **b, int **c, int n, int m, int q,
                           int cn_stream);


void first_part_mulH(int *mulH, int **a, int i_b, int i_e, int m);
void first_part_mulV(int *mulV, int **b, int i_b, int i_e, int m);
void third_part(int **a, int **b, int **c, int *mulH, int *mulV,
                int i_b, int i_e, int q, int m);

#endif // MULTI_H
