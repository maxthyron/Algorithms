def func():
    array = [[1, 5, 2], [-5, 3, 1], [8, 3, 2]]
    length = len(array)                          # 1
    flip = True                                  # 2
    counter1 = 0                                 # 3
    counter2 = 0                                 # 4
    flag1 = False                                # 5 
    flag2 = False                                # 6

    for i in range(length):                      # 7
        for j in range(length):                  # 8
            if array[i][j] % 2 == 0:             # 9
                counter1 += 1                    # 10
            else:                                # 11
                counter2 += 1                    # 12
            
            if array[i][j] < 0:                  # 13
                flip = False                     # 14
                flag1 = True                     # 15
            else:                                # 16
                flip = True                      # 17
                flag2 = True                     # 18
            t = array[i][j]                      # 19
            array[i][j] = array[j][i]            # 20
            array[j][i] = t                      # 21

    if flag1 and flag2:                          # 22
        print("You won")                         # 23
    else:                                        # 24
        print("You lose")                        # 25
